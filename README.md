YouTube Transcripts Quiz Maker
This Streamlit-based Allows users to input a youtube video link, extract the video's transcripts, 
and automatically generate a quiz based on those transcripts.

Getting Started
Clone the repository:
https://gitlab.com/genai-poc/poc5-youtube-transcripts-quiz-maker.git

Ensure you have the required dependencies installed:
pip install google-generativeai
pip install streamlit
pip install youtube-transcript-api

Usage
1, Create your own Gemini API key from here
https://aistudio.google.com/app/apikey

2. Upload a YouTube link:
Get one educational youtube video link and past it in input box

3, Generate Quiz button:
It will generate transcripts for particular youtube link and then generates quiz based on transcripts.

4, Answer quiz:
Select options from multiple choice questions and get your final score.



Running the Application:
Execute the following command in your terminal:
streamlit run app.py